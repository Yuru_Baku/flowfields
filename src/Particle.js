function getRandomInt(max){
    return Math.floor(Math.random() * max);
}

console.log("Init Particle class")
class Particle{
 constructor(effect){
    this.effect = effect;
    this.history = [];
    this.maxHistory = 1000;
    this.size = 8;
    this.resetParticle();
    this.updateHistory();
    
 }

 update(){
    //update speed
    const angle = this.effect.getAngle(this.x,this.y)
    this.speedX = Math.cos(angle);
    this.speedY = Math.sin(angle);

    //update position
    this.x += this.speedX;
    this.y += this.speedY;


    //bound check
    if(this.IsOutOfBounds()){
        this.resetParticle()
    }

    this.updateHistory();
 }

 draw(context){
    context.beginPath();
    const firstPos = this.history[0];
    context.moveTo(firstPos.x,firstPos.y);
    for(let i = 1; i < this.history.length; i++){
        const pos = this.history[i];
        context.lineTo(pos.x,pos.y)
    }
    context.stroke();
}

updateHistory(){
    const history = this.history;
    history.push({x:this.x,y:this.y});
    if(history.length > this.maxHistory){
        history.shift();
    }
 }

 resetParticle(){
    this.x = getRandomInt( this.effect.width);
    this.y = getRandomInt( this.effect.height);
    this.speedX = 1;
    this.speedY = 1;
    this.history = [];
 }

 IsOutOfBounds(){
    const xNotInBounds = this.x < 0 || this.x > this.effect.width;
    const yNotInBounds = this.y < 0 || this.y > this.effect.height;
    return xNotInBounds || yNotInBounds;

 }

}