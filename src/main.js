
//get canvas
const canvas = document.getElementById('canvas1');
const ctx = canvas.getContext('2d');

//canvas setup
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;
ctx.fillStyle='white';
ctx.strokeStyle='white';
ctx.lineWidth=1;
ctx.lineCap='round';
console.log(ctx);

const effect = new Effect(canvas.width,canvas.height)
console.log(effect);

function animate(){
    //ctx.clearRect(0,0,canvas.width,canvas.height);
    effect.render(ctx);
    effect.update();
    requestAnimationFrame(animate);
}

animate()