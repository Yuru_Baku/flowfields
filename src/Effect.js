console.log("Init Effect class")

class Effect{
    constructor(width,height){
        this.width = width;
        this.height = height;
        this.particles = [];
        this.flowField = new FlowField(5,width,height);
        this.numberOfParticles = 1000;
        this.init()
    }

    init(){
        console.log("Init new Particles")
        for(let i = 0; i < this.numberOfParticles; i++){
            this.particles.push(new Particle(this));
        }
    }

    getAngle(x,y){
        return this.flowField.getAngle(x,y);
    }

    update(){
        this.particles.map(particle => particle.update());
    }

    render(context){
        console.log("draw all particles");
        this.particles.map(particle => particle.draw(context));
    }
}