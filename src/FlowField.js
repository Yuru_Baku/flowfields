
console.log("Init FlowField class")

class FlowField{
    constructor(cellSize,width,height){
        this.cellSize = cellSize;
        this.widht = width;
        this.height = height;
        this.curve = 10;
        this.zoom = 0.1;
        this.init();
        console.log(this.field);
    }

    init(){
        this.rows = Math.floor(this.height/this.cellSize);
        this.cols = Math.floor(this.widht/this.cellSize);
        this.field = [];
        this.generateField();
    }

    getAngle(x,y){
        const index = this.getIndex(x,y);
        const angle = this.field[index];
        return angle;
    }

    getIndex(x,y){
        const rowNumber = Math.floor(y/this.cellSize);
        const colNumber = Math.floor(x/this.cellSize);
        const index = this.rows * rowNumber + colNumber;
        return index;
    }
    
    generateField(){
        for(let y = 0; y < this.rows; y++){
            for(let x = 0; x < this.rows; x++){
                const angle = (Math.cos(x*this.zoom) + Math.sin(y*this.zoom))*this.curve;
                this.field.push(angle);
            }
        }
    }

}